import './main.html';

// meteor client import
import '../imports/startup/both';
import '../imports/startup/client';

// import of vue components
import './vue/imports'

// vue configurations
import Vue from 'vue';
import router from './vue/routers'
import store from './vue/stores'
import App from './vue/App.vue';


Meteor.startup(() => {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
});
