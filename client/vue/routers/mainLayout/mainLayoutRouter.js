import MainLayout from '../../layouts/MainLayout.vue';
import Dashboard from '../../pages/dashboard/Dashboard.vue'
import UsersDashboard from '../../pages/users/UsersDashboard.vue'
// import StudentsDashboard from '../../pages/students/StudentsDashboard.vue'
import DepartmentsDashboard from '../../pages/departments/DepartmentsDashboard.vue'
import StudentsDashboard from '../../pages/students/StudentsDashboard.vue'
import StudentDetailDashboard from '../../pages/students/StudentDetailDashboard.vue'
import InvitationsDashboard from '../../pages/invitations/InvitationsDashboard.vue'

export default [
  {
    path: '/',
    component: MainLayout,
    children: [
      {
        path: '',
        name: 'dashboard',
        component: Dashboard,
        meta: {
          tabName: 'dashboard'
        }
      },
      // {
      //   path: '/students',
      //   name: 'students-dashboard',
      //   component: StudentsDashboard,
      //   meta: {
      //     tabName: 'students'
      //   }
      // },
      {
        path: '/departments',
        name: 'departments-dashboard',
        component: DepartmentsDashboard,
        meta: {
          tabName: 'departments'
        }
      },
      {
        path: '/students',
        name: 'students-dashboard',
        component: StudentsDashboard,
        meta: {
          tabName: 'students'
        }
      },
      {
        path: '/students/:studentId',
        name: 'student-detail-dashboard',
        component: StudentDetailDashboard,
        meta: {
          tabName: 'students'
        }
      },
      {
        path: '/invitations',
        name: 'invitations-dashboard',
        component: InvitationsDashboard,
        meta: {
          tabName: 'invitations'
        }
      },
      {
        path: '/users',
        name: 'users-dashboard',
        component: UsersDashboard,
        meta: {
          tabName: 'users'
        }
      },
    ]
  },
]
