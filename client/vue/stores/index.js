import Vue from 'vue'
import Vuex from 'vuex'

import mainLayoutModule from './layouts/mainLayoutModule'
import usersModule from './models/usersModule'
import departmentsModule from './models/departmentsModule'
import invitationsModule from './models/invitationsModule'
import studentsModule from './models/studentsModule'

Vue.use(Vuex)
export default new Vuex.Store({
  modules: {
    mainLayoutModule,
    usersModule,
    departmentsModule,
    invitationsModule,
    studentsModule
  }
})
