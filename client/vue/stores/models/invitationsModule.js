import { httpErrorHandler } from "../../handler";

export default {
  namespaced: true,
  state: {
  },
  mutations: {
  },
  actions: {
    async createInvitation(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "invitations.insert",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    async updateInvitation(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "invitations.update",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    async deleteInvitation(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "invitations.remove",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },

  }
}
