import { httpErrorHandler } from "../../handler";

export default {
  namespaced: true,
  state: {
  },
  mutations: {
  },
  actions: {
    async createStudent(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "students.insert",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    async updateStudent(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "students.update",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    async deleteStudent(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "students.remove",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },

  }
}
