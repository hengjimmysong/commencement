import { httpErrorHandler } from "../../handler";

export default {
  namespaced: true,
  state: {
  },
  mutations: {
  },
  actions: {
    async createDepartment(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "departments.insert",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    async updateDepartment(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "departments.update",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    async deleteDepartment(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.call(
          "departments.remove",
          { ...payload },
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },

  }
}
