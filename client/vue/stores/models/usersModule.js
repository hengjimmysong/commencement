import { httpErrorHandler } from "../../handler";

export default {
  namespaced: true,
  state: {
  },
  mutations: {
  },
  actions: {
    async logIn(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.loginWithPassword(
          payload.username,
          payload.password,
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
   
    async logOut(context, payload) {
      return new Promise((resolve, reject) => {
        Meteor.logout(
          (error, result) => {
            error ? httpErrorHandler(error) : resolve(true);
          }
        )
      })
    },
    
  }
}
