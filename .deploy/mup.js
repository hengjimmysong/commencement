module.exports = {
  servers: {
    one: {
      // TODO: set host address, username, and authentication method
      host: '104.248.145.85',
      username: 'root',
      pem: '~/.ssh/id_rsa'
      // password: 'server-password'
      // or neither for authenticate from ssh-agent
    }
  },

  app: {
    // TODO: change app name and path
    name: 'commencement',
    path: '../',

    volumes: {
      '/public/uploads': '/public/uploads'
    },

    servers: {
      one: {},
    },

    buildOptions: {
      serverOnly: true,
    },

    env: {
      // TODO: Change to your app's url
      // If you are using ssl, it needs to start with https://
      ROOT_URL: 'http://104.248.145.85',
      PORT: 3000
    },

    docker: {
      // change to 'abernix/meteord:base' if your app is using Meteor 1.4 - 1.5
      image: 'abernix/meteord:node-12-base',
      imagePort: 3001
    },

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true,

    // ssl: {
    //   autogenerate: {
    //     email: 'co-newskh@gmail.com',
    //     domains: 'co-newskh.com,www.co-newskh.com'
    //   }
    // }
  },

  mongo: {
    version: '3.4.1',
    port: 27018,
    servers: {
      one: {}
    }
  },

  // (Optional)
  // Use the proxy to setup ssl or to route requests to the correct
  // app when there are several apps

  // proxy: {
  //   domains: 'co-newskh.com,www.co-newskh.com',

  //   ssl: {
  //     // Enable Let's Encrypt
  //     letsEncryptEmail: 'co-newskh@gmail.com',
  //     forceSSL: true
  //   }
  // }
};
