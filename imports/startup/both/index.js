// Models
import "../../api/departments/departments"
import "../../api/students/students"
import "../../api/invitations/invitations"

// Methods
import "../../api/departments/methods"
import "../../api/students/methods"
import "../../api/invitations/methods"
