import { Meteor } from 'meteor/meteor';
import { Accounts } from "meteor/accounts-base"

var roles = ['admin', 'user']

roles.forEach(function (role) {
  Roles.createRole(role, { unlessExists: true });
});

// console.log(Meteor.absolutePath)

if (Meteor.users.find({ username: 'admin' }).fetch().length === 0) {

  console.log('Creating admin:');

  var users = [
    { username: "admin", email: "admin@gmail.com", roles: ['admin'] },
  ];



  users.forEach(function (userData) {
    var id,
      user;

    id = Accounts.createUser({
      username: userData.username,
      email: userData.email,
      password: "adminadmin",
      profile: {
        phone: "012121212",
        likedPosts: [],
        savedPosts: []
      }
    });

    // email verification
    Meteor.users.update({ _id: id }, { $set: { 'emails.0.verified': true } });

    Roles.setUserRoles(id, userData.roles);
  });
}

if (true) {
  const users = Meteor.users.find({});

  users.forEach((user) => {
    Meteor.users.update({ _id: user._id }, { $set: { 'profile.nickname': user.username } })
  })
}
