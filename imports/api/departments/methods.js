import { Meteor } from "meteor/meteor";
import { check } from 'meteor/check';

import Departments, { DepartmentsImages } from "./departments";


Meteor.methods({
  'departments.insert': (departments) => {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }

    return Departments.insert(departments);
  },
  'departments.update'(payload) {
    console.log(payload)
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }
    
    const { departmentId, name, code } = payload

    check(departmentId, String);
    check(name, String);
    check(code, String);

    return Departments.update(departmentId, { $set: { name, code } });
  },
  'departments.remove'(payload) {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }
    const { departmentId } = payload

    check(departmentId, String);

    Departments.remove(departmentId);
  }
})
