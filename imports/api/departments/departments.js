import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { options, timetableSchema } from '../schema';

const Departments = new Mongo.Collection('departments');

let DepartmentSchema = new SimpleSchema({
  name: {
    type: String,
    required: true,
  },
  code: {
    type: String,
    required: true,
  }
}, {
  ...options
});
DepartmentSchema.extend(timetableSchema);

Departments.attachSchema(DepartmentSchema);

export default Departments;

Departments.helpers({

})
