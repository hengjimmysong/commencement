import Departments from './departments';
import { publishComposite } from 'meteor/reywood:publish-composite';


publishComposite('departments', {
  find() {
      return Departments.find({});
  },
});

