import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { options, timetableSchema } from '../schema';

const Attendances = new Mongo.Collection('attendances');

let AttendanceSchema = new SimpleSchema({
  invitationId: {
    type: String,
    required: true,
  },
  picture: {
    type: String,
    required: false
  },
}, {
  ...options
});
AttendanceSchema.extend(timetableSchema);

Attendances.attachSchema(AttendanceSchema);

export default Attendances;

Attendances.helpers({

})
