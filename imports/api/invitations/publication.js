import { publishComposite } from 'meteor/reywood:publish-composite';
import Invitations from './invitations';
import Students from '../students/students';


publishComposite('invitations', {
  find() {
    return Invitations.find({});
  },
  children: [
    {
      find(invitation) {
        return Students.find({ _id: invitation.studentId })
      }
    }
  ]
});

