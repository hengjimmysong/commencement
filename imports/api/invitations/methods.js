import { Meteor } from "meteor/meteor";
import { check } from 'meteor/check';

import Invitations from "./invitations";


Meteor.methods({
  'invitations.insert': (invitations) => {
    return Invitations.insert(invitations);
  },
  'invitations.update'(payload) {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }

    const { invitationId, guestName, studentId, relationship, gender } = payload

    check(invitationId, String);
    check(name, String);
    check(code, String);

    return Invitations.update(invitationId, { $set: { guestName, studentId, relationship, gender } });
  },
  'invitations.remove'(payload) {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }
    const { invitationId } = payload

    check(invitationId, String);

    Invitations.remove(invitationId);
  }
})
