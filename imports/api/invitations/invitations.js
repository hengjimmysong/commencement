import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { options, timetableSchema } from '../schema';
import Students from '../students/students';

const Invitations = new Mongo.Collection('invitations');

let InvitationSchema = new SimpleSchema({
  guestName: {
    type: String,
    required: true,
  },
  studentId: {
    type: String,
    required: true
  },
  relationship: {
    type: String,
    required: false
  },
  gender: {
    type: String,
    required: false,
  },
}, {
  ...options
});
InvitationSchema.extend(timetableSchema);

Invitations.attachSchema(InvitationSchema);

export default Invitations;

Invitations.helpers({
  student() {
    return Students.findOne({ _id: this.studentId })
  }
})
