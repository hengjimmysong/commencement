import { Meteor } from "meteor/meteor";
import { check } from 'meteor/check';

import Students, { StudentsImages } from "./students";
import Invitations from "../invitations/invitations";


Meteor.methods({
  'students.insert': (students) => {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }

    return Students.insert(students);
  },
  'students.update'(payload) {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }

    const { studentId, fullName, idNumber, email, schoolEmail, gender, phoneNumber, departmentId } = payload

    return Students.update(studentId, { $set: { fullName, idNumber, email, schoolEmail, gender, phoneNumber, departmentId } });
  },
  'students.remove'(payload) {
    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized!')
    }

    const { studentId } = payload

    check(studentId, String);

    Students.remove(studentId);
    Invitations.remove({ studentId })
  }
})
