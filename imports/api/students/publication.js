import Students from './students';
import { publishComposite } from 'meteor/reywood:publish-composite';
import Invitations from '../invitations/invitations';
import Departments from '../departments/departments';


publishComposite('students', {
  find() {
    return Students.find({});
  },
  children: [
    {
      find(student) {
        return Invitations.find({ studentId: student._id })
      }
    },
    {
      find(student) {
        return Departments.find({ _id: student.departmentId })
      }
    },
  ]
});
