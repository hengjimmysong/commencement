import { Mongo } from 'meteor/mongo';
import SimpleSchema from 'simpl-schema';
import { options, timetableSchema } from '../schema';
import Invitations from '../invitations/invitations';
import Departments from '../departments/departments';

const Students = new Mongo.Collection('students');

let StudentSchema = new SimpleSchema({
  fullName: {
    type: String,
    required: true,
  },
  idNumber: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: false
  },
  schoolEmail: {
    type: String,
    required: false
  },
  gender: {
    type: String,
    required: false,
  },
  phoneNumber: {
    type: String,
    required: false,
  },
  departmentId: {
    type: String,
    required: true
  },
}, {
  ...options
});
StudentSchema.extend(timetableSchema);

Students.attachSchema(StudentSchema);

export default Students;

Students.helpers({
  invitations() {
    return Invitations.find({ studentId: this._id })
  },
  department() {
    console.log(Departments.findOne({ _id: this.departmentId }))
    return Departments.findOne({ _id: this.departmentId })
  }
})
