import SimpleSchema from 'simpl-schema';

export const timetableSchema = new SimpleSchema({
  createdAt: {
    type: Date,
    autoValue: function () {
      if (this.isInsert) {
        return new Date();
      }
    },
  },
  updatedAt: {
    type: Date,
    autoValue: function () {
      if (this.isUpdate || this.isInsert) {
        return new Date();
      }
    },
  },
});

export const options = {
  clean: {
    trimStrings: true
  }
}
