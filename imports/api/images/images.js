import { FilesCollection } from 'meteor/ostrio:files';

const Images = new FilesCollection({
  // Prod
  storagePath: Meteor.isProduction ? '/data/uploads' : `${Meteor.absolutePath}/public/uploads`,
  permissions: 0o774,
  parentDirPermissions: 0o774,
  collectionName: 'Images',
  allowClientCode: false,
  onBeforeUpload: function (file) {
    // Allow upload files under 10MB, and only in png/jpg/jpeg formats
    if (file.size <= 1024 * 1024 * 10 && /png|jpg|jpeg/i.test(file.extension)) {
      return true;
    } else {
      return 'Please upload image, with size equal or less than 10MB';
    }
  }
});

export default Images;
