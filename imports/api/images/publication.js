import { publishComposite } from 'meteor/reywood:publish-composite';
import Images from './images';


publishComposite('images', {
  find() {
    return Images.collection.find({})
  }
})
