import { Meteor } from 'meteor/meteor';

var Users = Meteor.users;

Users.helpers({
  isAdmin() {
    return Roles.userIsInRole(this._id, 'admin')
  },
  isModerator() {
    return Roles.userIsInRole(this._id, 'moderator')
  },
})
