import { Meteor } from "meteor/meteor";
import { publishComposite } from 'meteor/reywood:publish-composite';

publishComposite('users', {
  find() {
    return Meteor.users.find({});
  },
  children: [
    {
      find(user) {
        return Meteor.roleAssignment.find({});
      }
    },
  ]
});
