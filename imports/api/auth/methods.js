import { Meteor } from "meteor/meteor";
import { Accounts } from "meteor/accounts-base";
import { check } from 'meteor/check';


Meteor.methods({
  'users.create': (payload) => {
    const { username, password, phone, email, name } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'moderator')) {
      throw new Meteor.Error('401', 'You are not authorized to create user!')
    }

    return Accounts.createUser({ username, password, profile: { phone, email, name } });
  },
  'users.update': (payload) => {
    const { userId: targetUserId, phone, username, email, name } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized to update user!')
    }

    return Meteor.users.update({ _id: targetUserId }, {
      $set:
      {
        username,
        'profile.phone': phone,
        'profile.email': email,
        'profile.name': name,

      }
    });
  },
  'users.image': (payload) => {
    const { userId: targetUserId, image } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin') && userId !== targetUserId) {
      throw new Meteor.Error('401', 'You are not authorized to update user!')
    }

    return Meteor.users.update({ _id: targetUserId }, {
      $set:
      {
        'profile.image': image,
      }
    });
  },
  'users.makeAdmin': async (payload) => {
    const { userId: targetUserId } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized to make admins!')
    }

    Roles.createRole('admin', { unlessExists: true });
    const role = await Roles.setUserRoles(targetUserId, 'admin');

    return;
  },
  'users.makeModerator': async (payload) => {
    const { userId: targetUserId } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized to make moderators!')
    }

    Roles.createRole('moderator', { unlessExists: true });
    const role = await Roles.setUserRoles(targetUserId, 'moderator');

    return;
  },
  'users.removeRole': (payload) => {
    const { userId: targetUserId } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized to remove roles from user!')
    }

    Roles.setUserRoles(targetUserId, []);

    return;
  },
  'users.remove': (payload) => {
    const { userId: targetUserId } = payload;

    const userId = Meteor.userId();

    if (!Roles.userIsInRole(userId, 'admin')) {
      throw new Meteor.Error('401', 'You are not authorized to remove user!')
    }

    if (!targetUserId) {
      throw new Meteor.Error('422', 'No user id was given!')
    }

    if (userId == targetUserId) {
      throw new Meteor.Error('422', 'Can\'t remove current user!')
    }

    return Meteor.users.remove(targetUserId);
  }
})
